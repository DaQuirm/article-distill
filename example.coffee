distill = require './src/distill.coffee'
article = new distill.Article()
# distill.scrape 'http://www.capitalbay.com/uk/300475-the-sealed-bottle-garden-still-thriving-after-40-years-without-fresh-air-or-water.html', 'article', (data) ->
# distill.scrape 'http://www.scientificamerican.com/article.cfm?id=silver-makes-antibiotics-thousands-of-times-more-effective', 'article', (data) ->
distill.scrape 'http://www.wired.com/wiredenterprise/2013/06/yahoo-amazon-amplab-spark/all/', 'article', (data) ->
	article.resources.info.link = data.link
	article.resources.info.title = data.title
	article.resources.images = data.image_filenames.map (filename) -> file:filename
	article.article.sections = [{ items: data.content_items }]
	terms = distill.tag data.content_items
		.filter((item) -> item instanceof distill.Paragraph)
		.map((item) -> item.content[0].content)
		.join ' '
	article.resources.tags = terms[0...10].map (term) -> term.term
	article.save 'article/article.json'
	distill.pack 'article', "#{data.title}.article", ->

