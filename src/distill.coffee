jsdom 		= require 'jsdom'
http  		= require 'http'
fs    		= require 'fs'
path  		= require 'path'
readability = require 'node-readability'
async 		= require 'async'
{exec} 		= require 'child_process'
natural		= require 'natural'
request		= require 'request'
url 		= require 'url'

distill = module.exports

distill.scrape = (article_url, dir, done) ->
	fs.mkdirSync dir if not fs.existsSync dir
	readability.read article_url, (err, article) ->
		content = do article.getContent
		article_href = (url.parse article_url).href
		jsdom.env content, (errors, window) ->
			articleNode = window.document.documentElement
			content_items = []
			image_urls = []
			image_index = 0
			content_items = (Array::slice.call articleNode.querySelectorAll('p, img')).map (node) ->
				if node.nodeName is 'IMG'
					src = url.resolve article_href, (node.getAttribute 'src')
					console.log src
					image_urls.push src
					image = new Image
					image.index = image_index++
					image
				else if node.nodeName is 'P'
					new Paragraph node.textContent

			async.each(
				image_urls
				(image_url, done) ->
					if /base64/.test image_url
						base64_data = image_url.match(/base64,(.+)/)[1]
						fs.writeFile "base64-image-.png", base64_data, 'base64', (err) ->
							console.log err
							do done
					else
						file = fs.createWriteStream path.join(dir, path.basename image_url)
						file.on 'finish', ->
							console.log "Done with #{image_url}"
							do file.close
							do done
						console.log "Piping #{image_url}"
						request(image_url).pipe file
				->
					done
						link: article_url
						content_items: content_items
						image_filenames: image_urls.map path.basename
						title: do article.getTitle
				)

distill.pack = (directory, filename, done) ->
	exec "zip -jr \"#{filename}\" \"#{directory}\"/*", (error, stdout, stderr) ->
		do done

distill.tag = (text) ->
	tfidf = new natural.TfIdf()
	tfidf.addDocument text
	tfidf.listTerms 0

distill.Article = class Article
	constructor: ->
		@resources =
			info :
				link: ''
				source: ''
				title: ''
				subtitle: ''
				author: ''
				date: ''
				category: ''
				version: '1.0.0'
			footnotes: []
			images: []
			tags: []
			stylesheets: []
		@article =
			width: 700
			sections: []
		@comments = []

	save: (filename = 'article.json') ->
		fs.writeFileSync filename, JSON.stringify(@, null, 4)

distill.Paragraph = class Paragraph
	constructor: (text) ->
		@type = 'paragraph'
		@content = [
			type: 'text'
			content: text
		]

distill.Image = class Image
	constructor: ->
		@type = 'image'

